config_hadoop_env(){
# configure java path in hadoop-env.sh
  sed -i 's!export JAVA_HOME=.*$!export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64!g' "$HADOOP_HOME/etc/hadoop/hadoop-env.sh"
  return $?
}


config_core_site(){
# core-site.xml contains:
#  - port number used for hadoop instance
#  - memory allocated for the file system
#  - memory limit for storing the data
#  - size of r/w buffers
   sed -i '/<configuration>$/,/<\/configuration>$/d' $HADOOP_HOME/etc/hadoop/core-site.xml
cat <<ENDCORESITECONFIG >> $HADOOP_HOME/etc/hadoop/core-site.xml
<configuration>
  <property>
     <name>fs.default.name</name>
     <value>hdfs://localhost:9000</value>
  </property>
  <property>
     <name>hadoop.tmp.dir</name>
     <value>file:$HDFSPATH/tmp</value>
     <description>A base for other temporary directories.</description>
  </property>
</configuration>
ENDCORESITECONFIG
  return $?
}


config_hdfs_site(){
# hdfs-site.xml contains:
#  - replication factor
#  - namenode path
#  - datanode path of our local infra (place were to store hadoop infra)
#  - size of r/w buffers
 sed -i '/<configuration>$/,/<\/configuration>$/d' $HADOOP_HOME/etc/hadoop/hdfs-site.xml
cat <<ENDHDFSSITECONFIG >> $HADOOP_HOME/etc/hadoop/hdfs-site.xml
<configuration>
   <property>
      <name>dfs.replication</name>
      <value>1</value>
   </property>
   <property>
      <name>dfs.name.dir</name>
      <value>file:$NAMENODEPATH</value>
   </property>
   <property>
      <name>dfs.data.dir</name>
      <value>file:$DATANODEPATH</value>
   </property>
</configuration>
ENDHDFSSITECONFIG
  return $?
}


config_yarn_site(){
# yarn configuration file
  if [ ! -f $HADOOP_HOME/etc/hadoop/yarn-site.xml ] ; then
    cp $HADOOP_HOME/etc/hadoop/yarn-site.xml.template $HADOOP_HOME/etc/hadoop/yarn-site.xml
  fi
  sed -i '/<configuration>$/,/<\/configuration>$/d' $HADOOP_HOME/etc/hadoop/yarn-site.xml
cat <<ENDYARNSITECONFIG >> $HADOOP_HOME/etc/hadoop/yarn-site.xml
<configuration>
   <property>
      <name>yarn.nodemanager.aux-services</name>
      <value>mapreduce_shuffle</value>
   </property>
   <property>
      <name>yarn.scheduler.minimum-allocation-mb</name>
      <value>4608</value>
   </property>
   <property>
      <name>yarn.scheduler.maximum-allocation-mb</name>
      <value>13824</value>
   </property>
   <property>
      <name>yarn.nodemanager.resource.memory-mb</name>
      <value>13824</value>
   </property>
</configuration>
ENDYARNSITECONFIG
  return $?
}


config_mapred_site(){
# yarn configuration file
  if [ ! -f $HADOOP_HOME/etc/hadoop/mapred-site.xml ] ; then
    cp $HADOOP_HOME/etc/hadoop/mapred-site.xml.template $HADOOP_HOME/etc/hadoop/mapred-site.xml
  fi
  sed -i '/<configuration>$/,/<\/configuration>$/d' $HADOOP_HOME/etc/hadoop/mapred-site.xml
cat <<ENDMAPREDSITECONFIG >> $HADOOP_HOME/etc/hadoop/mapred-site.xml
<configuration>
   <property>
      <name>mapreduce.framework.name</name>
      <value>yarn</value>
   </property>
   <property>
      <name>mapreduce.map.memory.mb</name>
      <value>2304</value>
   </property>
   <property>
      <name>mapreduce.map.java.opts</name>
      <value>-Xmx1843m</value>
   </property>
   <property>
      <name>mapreduce.reduce.memory.mb</name>
      <value>4608</value>
   </property>
   <property>
      <name>mapreduce.reduce.java.opts</name>
      <value>-Xmx3686m</value>
   </property>
   <property>
      <name>yarn.app.mapreduce.am.resource.mb</name>
      <value>2304</value>
   </property>   
   <property>
      <name>yarn.app.mapreduce.am.command-opts</name>
      <value>-Xmx1843m</value>
   </property>   
   <property>
      <name>mapreduce.task.io.sort.mb</name>
      <value>921</value>
   </property>
</configuration>
ENDMAPREDSITECONFIG
  return $?
}
