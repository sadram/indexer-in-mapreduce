YesOrNo ()
{
    while true; do
        echo -n "$@"
        echo -n " (Y/N)? "
        if ! read ans ; then return 1 ; fi
        case $ans in
            [oOyY]* ) return 0 ;;
            [nN]* ) return 1 ;;
        esac
    done
}
 
make_hadoop_infra(){
# create namenode and datanode path
 if [ ! -d "$HDFSPATH" ] ; then
    mkdir -p "$NAMENODEPATH" "$DATANODEPATH"
    mkdir -p "$HDFSTMP"
    chmod 750 "$HDFSTMP"
  fi
}


add_to_path() {
# safely add a path to the PATH environment variable
  case ":$PATH:" in
      *":$1:"*) :;; # already here
      *) PATH="$PATH:$1" ;;
  esac
  export PATH
  return $?
}


fetch_hadoop() {
  test -d ${TARGETDIR} || (echo -ne "Creating ${TARGETDIR} ..." && mkdir -p ${TARGETDIR} && echo "OK!")
  echo -ne "Fetching ${KEYSFILE} ... "  && wget -q "${BASE_URL}/${KEYSFILE}" -O ${TARGETDIR}/${KEYSFILE}  && echo 'OK!\n'
  echo -ne "Fetching ${HADOOPSRC} ... " && wget -q "${MIRROR_URL}/${HADOOPBASENAME}/${HADOOPSRC}" -O ${TARGETDIR}/${HADOOPSRC} && echo 'OK!\n'
  echo -ne "Fetching ${HADOOPASC} ... " && wget -q "${BASE_URL}/${HADOOPBASENAME}/${HADOOPASC}" -O ${TARGETDIR}/${HADOOPASC} && echo 'OK!\n'
  echo -ne "Fetching ${HADOOPMDS} ... " && wget -q "${MIRROR_URL}/${HADOOPBASENAME}/${HADOOPMDS}" -O ${TARGETDIR}/${HADOOPMDS} && echo 'OK!\n'
  echo -ne "Fetching ${HADOOPBIN} ... " && wget -q "${MIRROR_URL}/${HADOOPBASENAME}/${HADOOPBIN}" -O ${TARGETDIR}/${HADOOPBIN} && echo 'OK!\n'
}


unpack_and_check_bin() {
  cat ${TARGETDIR}/${HADOOPMDS}
  $(readlink -e $(which sha1sum)) ${TARGETDIR}/${HADOOPBIN}
  $(readlink -e $(which sha256sum)) ${TARGETDIR}/${HADOOPBIN}
  $(readlink -e $(which sha512sum)) ${TARGETDIR}/${HADOOPBIN}
  if YesOrNo 'if the sum is correct (yes to extract the archive)?' ;  then
    tar -xvzf ${TARGETDIR}/${HADOOPBIN} -C ${PROJBINHOME}
  fi
  echo "Creating symlink hadoop -> ${PROJBINHOME}/${HADOOPBASENAME}"
  ln -sf "${PROJBINHOME}/${HADOOPBASENAME}" "${PROJBINHOME}/hadoop"
  # gpg --import ${KEYSFILE} 
  # gpg --refresh-keys
  # gpg --verify ${TARGETDIR}/${HADOOPASC}
}

