#    http://localhost:50070/ – web UI of the NameNode daemon
#    http://localhost:50030/ – web UI of the JobTracker daemon
#    http://localhost:50060/ – web UI of the TaskTracker daemon

# disable IPv6 (if usigin 0.0.0.0 as listen adresse hadoop defaults to IPv6)
# optional if HADOOP_OPTS=-Djava.net.preferIPv4Stack=true is set in hadoop-env.sh
#sudo sysctl net.ipv6.conf.all.disable_ipv6=1
#sudo sysctl net.ipv6.conf.default.disable_ipv6=1
#sudo sysctl net.ipv6.conf.lo.disable_ipv6=1
#sudo echo 'net.ipv6.conf.all.disable_ipv6=1' >> /etc/sysctl.conf
#sudo echo 'net.ipv6.conf.default.disable_ipv6=1' >> /etc/sysctl.conf
#sudo echo 'net.ipv6.conf.lo.disable_ipv6=1' >> /etc/sysctl.conf
# should return 0
#cat /proc/sys/net/ipv6/conf/all/disable_ipv6 

# create a hadoop user and group and allow it to connect in ssh withoug pass
sudo addgroup hadoop
sudo adduser --ingroup hadoop hduser
# add hadoop binaries to path for hduser
echo 'export PATH=${PATH}:${HADOOP_HOME}/sbin:${HADOOP_HOME}/bin' >> .bashrc

su -p -c ssh-keygen -t rsa -P "" hduser
su -p -c 'cat /home/hadoop/.ssh/id_rsa.pub >> /home/hadoop/.ssh/authorized_keys'

# change the owner of the hadoop untared archive
sudo chown -R hduser:hadoop hadoop

# format the namenode
# TODO: BEWARE in case of hadoop reinstall delete the $HDFSPATH (hadoop infra)
# TODO: avoid ClusterID conflict in hdfs/datanode/current/VERSIOn
sudo -u hduser hdfs namenode -format

# start hadoop
sudo -u hduser start-all.sh

# create a home directory
# hdfs dfs -mkdir -p /user/$(whomai)
hdfs dfs -mkdir -p /user/hduser

# test a quick wordcount
sudo -u hduser hadoop dfs -copyFromLocal ../txt /user/hduser/gutenberg
sudo -u hduser hadoop dfs -ls /user/hadoop/gutenberg
# hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.2.jar
# if jobs won't are accepted but don't run it may be because fs is full 90%
sudo -u hduser hadoop jar hadoop-mapreduce-examples-2.7.2.jar wordcount /user/hduser/gutenberg /user/hduser/gutenberg-output
sudo -u hduser hadoop dfs -cat /user/hduser/gutenberg-output/part-r-00000
sudo -u hduser mkdir gutenberg-output
sudo -u hduser hadoop dfs -getmerge /user/hduser/gutenberg-output gutenberg-output
sudo -u hduser cat gutenberg-output/gutenberg-output

sudo aptitude install -yR libgtest-dev autotools-dev automake autoconf cmake zlib1g-dev pkg-config libssl-dev protobuf-compiler snappy libsnappy-dev bzip2 liblzo2-dev libbz2-dev libjansson-dev fuse libfuse-dev
git clone https://github.com/google/protobuf.git
git checkout tags/v2.5.0 -b myprotobuff
./configure
make
make check
sudo make install
sudo ldconfig

cd bin/installers
tar xvzf hadoop-2.7.2-src.tar.gz
cd hadoop-2.7.2-src

# To fully rebuild hadoop could take a while :)
# mvn package -Pdist,native -DskipTests=true -Dtar

# to only rebuild native libs and include snappy compression
mvn package -Pdist,native -DskipTests -Dtar -Dmaven.javadoc.skip=true -Dsnappy.prefix=$HADOOP_HOME/snappy -Drequire.snappy=true

# keep a backup just in case
mv $HADOOP_HOME/lib/native{,_old}
cp -r $TARGETDIR/hadoop-2.7.2-src/hadoop-dist/target/hadoop-2.7.2/lib/native $HADOOP_HOME/lib
cp /usr/lib/x86_64-linux-gnu/libsnappy.so /usr/lib/x86_64-linux-gnu/libsnappy.a $HADOOP_HOME/lib/native

# check that everything is fine :)
hadoop checknative -a
# adoop checknative -a  
#16/05/29 19:40:15 INFO bzip2.Bzip2Factory: Successfully loaded & initialized native-bzip2 library system-native
#16/05/29 19:40:15 INFO zlib.ZlibFactory: Successfully loaded & initialized native-zlib library
#Native library checking:
#hadoop:  true /home/yaj/devel/DSBA/mdp/Assign1/bin/hadoop-2.7.2/lib/native/libhadoop.so.1.0.0
#zlib:    true /lib/x86_64-linux-gnu/libz.so.1
#snappy:  true /usr/lib/x86_64-linux-gnu/libsnappy.so.1
#lz4:     true revision:99
#bzip2:   true /lib/x86_64-linux-gnu/libbz2.so.1
#openssl: true /usr/lib/x86_64-linux-gnu/libcrypto.so

