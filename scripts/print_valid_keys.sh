awk '$1~/^pub/ && $4~/expires/ {
    gsub("]", "", $5)
    split($5, d, "-");
    /*print(d[1]" "d[2]" "d[3])*/
    if (d[1] > 2016 || (d[1] == 2016 && d[2] > 5) || (d[1] == 2016 && d[2] == 5 && d[3] >= 18))
    {
        gsub(".*/", "", $2)
        print($2)
    }
}' KEYS
