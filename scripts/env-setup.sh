export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export YARN_HOME=$HADOOP_HOME
#export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
#export HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=$HADOOP_HOME/lib"

export HDFSPATH=$PROJBINHOME/hadoopinfra/hdfs
export HDFSTMP=$HDFSPATH/tmp
export NAMENODEPATH=$HDFSPATH/namenode
export DATANODEPATH=$HDFSPATH/datanode

export BASE_URL="https://dist.apache.org/repos/dist/release/hadoop/common"
#export MIRROR_URL="http://apache.trisect.eu/hadoop/common"
export MIRROR_URL="http://apache.mirrors.ovh.net/ftp.apache.org/dist/hadoop/common"
export VERSION="2.7.2"
export NAME="hadoop"
export TGZ="tar.gz"
export ASC="${TGZ}.asc"
export MDS="${TGZ}.mds"
export HADOOPBASENAME="${NAME}-${VERSION}"
export HADOOPBIN="${HADOOPBASENAME}.${TGZ}"
export HADOOPSRC="${HADOOPBASENAME}-src.${TGZ}"
export HADOOPMDS="${HADOOPBASENAME}.${MDS}"
export HADOOPASC="${HADOOPBASENAME}.${ASC}"
export KEYSFILE="KEYS"
export TARGETDIR="${PROJHOME}/bin/installers"
