#!/bin/bash

export PROJHOME=$(cd -P -- "$(dirname "$0")" && pwd -P)
export PROJBINHOME=${PROJHOME}/bin
export SCRIPTDIR=${PROJHOME}/scripts
export HADOOP_HOME=${PROJBINHOME}/hadoop

# setup all the environment variables for HADOOP except the global PATH
if [ -f "${SCRIPTDIR}/env-setup.sh" ] ; then
    . "${SCRIPTDIR}/env-setup.sh"
else
   echo "${SCRIPTDIR}/env-setup.sh doesn't exist" >&2
   exit -1
fi

# source utilities and functions
if [ -f "${SCRIPTDIR}/helperFunctions.sh" ] ; then
   . "${SCRIPTDIR}/helperFunctions.sh"
else
   echo "${SCRIPTDIR}/helperFunctions.sh doesn't exist" >&2
   exit -1
fi

# safely add hadoop binaries to the PATH environment variable
add_to_path "${HADOOP_HOME}/sbin"
add_to_path "${HADOOP_HOME}/bin"

# source hadoop configuration functions
if [ -f "${SCRIPTDIR}/hadoopConfigFunctions.sh" ] ; then
   . "${SCRIPTDIR}/hadoopConfigFunctions.sh"
else
   echo "${SCRIPTDIR}/hadoopConfigFunctions.sh doesn't exist" >&2
   exit -1
fi

# setup hadoop
config_hadoop_env
config_core_site
make_hadoop_infra
config_hdfs_site
config_yarn_site
config_mapred_site
