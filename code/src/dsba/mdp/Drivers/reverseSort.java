package dsba.mdp.Drivers;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.map.InverseMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.Random;


/**
 * Created by @yaj
 *
 * @date: 30/05/16.
 */
public class reverseSort extends Configured implements Tool {
    private reverseSort() {
    }

    @Override
    public int run(String[] args) throws Exception {
        if(args.length < 2) {
            System.err.println("reverseSort <inputFile> <outputFile>");
            ToolRunner.printGenericCommandUsage(System.err);
            return 2;
        } else {
            Configuration config = this.getConf();
            FileSystem fs = FileSystem.get(config);
            Path inFile = new Path(args[0]);
            Path outFile = new Path(args[1]);

            // Check if the path already exists
            if (!(fs.exists(inFile))) {
                System.exit(-1);
            }
            if (fs.exists(outFile)) {
                System.exit(-1);
            }

            config.set("mapreduce.output.textoutputformat.separator", ",");
            config.set("mapreduce.input.keyvaluelinerecordreader.wordA.value.separator", ",");

            Job reverseSortJob = Job.getInstance(config);
            reverseSortJob.setJobName("reverse-sort");
            reverseSortJob.setJarByClass(reverseSort.class);

            FileInputFormat.addInputPath(reverseSortJob, inFile);
            Path tmpDir = new Path("reverseSort-temp-" + Integer.toString((new Random()).nextInt(2147483647)));

            FileOutputFormat.setOutputPath(reverseSortJob, tmpDir);

            reverseSortJob.setInputFormatClass(KeyValueTextInputFormat.class);
            reverseSortJob.setMapperClass(InverseMapper.class);
            reverseSortJob.setMapOutputKeyClass(Text.class);
            reverseSortJob.setMapOutputValueClass(Text.class);

            reverseSortJob.setNumReduceTasks(1);

            reverseSortJob.setSortComparatorClass(LongWritable.DecreasingComparator.class);
            try {
                reverseSortJob.waitForCompletion(true);
                FileUtil.copyMerge(fs, tmpDir, fs, outFile, false, config, null);
                fs.delete(tmpDir, true);
            } catch (InterruptedException | IOException | ClassNotFoundException Ex) {
                System.err.println(Ex);
            }
        }
        return 0;
    }




    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new reverseSort(), args);
        System.exit(res);
    }
}
