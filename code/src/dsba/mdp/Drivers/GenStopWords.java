package dsba.mdp.Drivers;

import dsba.mdp.Mappers.GenStopWordsMap;
import dsba.mdp.Reducers.GenStopWordsReduce;
import dsba.mdp.Reducers.IntSumReducer;
import org.apache.commons.cli.Options;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.map.InverseMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Date;
import java.util.Random;


/**
 * Created by @yaj
 *
 * @date: 25/05/16.
 */


public class GenStopWords extends Configured implements Tool {
    private static final Logger LOG_GenStopWords = Logger.getLogger(GenStopWords.class);
    private String mTextOutputSeparator;

    public String getTextOutputSeparator() {
        return this.mTextOutputSeparator;
    }

    public void setTextOutputSeparator(String textOutputSeparator) {
        this.mTextOutputSeparator = textOutputSeparator;
    }


    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new GenStopWords(), args);
        System.exit(res);
    }

    /**
     * Run different stop words finder scenari to assess the best choice.
     * Different setups include:
     *  - the number of reducers
     *  - on the usage of a combiner to speedup the Reduce task
     *  - the pertinence of a compression algorithm.
     *
     * Hadoop native support includes the following codecs:
     *  - Gzip
     *  - Lz4
     *  - Bzip2
     *  - Snappy
     */
    @Override
    public int run(String[] args) throws Exception {
        int retQa = runScenariQuestionA(args);

        return retQa;

    }

    private int runScenariQuestionA(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
        // with or without combiner
        int none_false_10 =     GenStopWordRunner(args, 10, false, "none");
        int none_true_10 =      GenStopWordRunner(args, 10, true,  "none");

        // Compression codec benchmark
        int Lz4_false_10 =      GenStopWordRunner(args, 10, false, "Lz4");
        int Lz4_true_10 =       GenStopWordRunner(args, 10, true,  "Lz4");
        int Snappy_false_10 =   GenStopWordRunner(args, 10, false, "Snappy");
        int Snappy_true_10 =    GenStopWordRunner(args, 10, true,  "Snappy");
        int Gzip_true_10 =      GenStopWordRunner(args, 10, true,  "Gzip");
        int Bzip2_true_10 =     GenStopWordRunner(args, 10, true,  "Bzip2");

        // increase number of reducer
        int none_false_50 =     GenStopWordRunner(args, 50, false, "none");
        int none_true_50 =      GenStopWordRunner(args, 50, true,  "none");
        int Snappy_true_50 =    GenStopWordRunner(args, 50, false, "Snappy");
        int Snappy_false_50 =   GenStopWordRunner(args, 50, true,  "Snappy");
        int Lz4_false_50 =      GenStopWordRunner(args, 50, false, "Lz4");
        int Lz4_true_50 =       GenStopWordRunner(args, 50, true,  "Lz4");

        return(none_false_10
                + none_true_10
                + Lz4_false_10
                + Lz4_true_10
                + Snappy_false_10
                + Snappy_true_10
                + Gzip_true_10
                + Bzip2_true_10
                + none_false_50
                + none_true_50
                + Snappy_false_50
                + Snappy_true_50
                + Lz4_false_50
                + Lz4_true_50
        );
    }

    /**
     * Setup and run a stop word generator.
     *
     */
    public int GenStopWordRunner(String[] args, int numReducer, boolean enableCombiner, String codecName) throws
            InterruptedException,
            IOException,
            ClassNotFoundException,
            IllegalArgumentException
    {
        if (args.length < 2) {
            System.out.println("GenStopWords <inDir> <outDir> [<opts>]");
            ToolRunner.printGenericCommandUsage(System.out);
            System.exit(-1);
        } else {
            
        /*
        * Options parsing
        * */
            Configuration conf = this.getConf();
            Options options = new Options();
            GenericOptionsParser optParser = new GenericOptionsParser(conf, options, args);
            String[] nonHadoopArgs = optParser.getRemainingArgs(); //Placeholder

            setTextOutputSeparator(",");
            conf.set("mapreduce.output.textoutputformat.separator", mTextOutputSeparator);
            String jobName =  "StopWords" + numReducer + "-" + enableCombiner + "-" + codecName;
            String jobOutputPath = args[1] + "/" + jobName;

            FileSystem fs = FileSystem.get(conf);
            Path inputPath = new Path(args[0]);
            Path GenStopWordJobOutputPath = new Path(jobOutputPath);

            // Check if the path already exists
            if (!(fs.exists(inputPath))) {
                LOG_GenStopWords.error("Path " + inputPath + " does not exists!");
                System.exit(-1);
            }
            if (fs.exists(GenStopWordJobOutputPath)) {
                LOG_GenStopWords.error("File " + GenStopWordJobOutputPath + " already exists!");
                System.exit(-1);
            }

        /*
        * StopWord generation
        */
            Job GenStopWordsJob = Job.getInstance(conf, jobName);
            GenStopWordsJob.setJarByClass(this.getClass());
            FileInputFormat.setInputPaths(GenStopWordsJob, inputPath);
            FileOutputFormat.setOutputPath(GenStopWordsJob, GenStopWordJobOutputPath);
            GenStopWordsJob.setMapperClass(GenStopWordsMap.class);
            GenStopWordsJob.setReducerClass(GenStopWordsReduce.class);
            GenStopWordsJob.setOutputKeyClass(Text.class);
            GenStopWordsJob.setOutputValueClass(IntWritable.class);
            LOG_GenStopWords.info(GenStopWordsJob.getConfiguration());

        /*
        * setups for the current scenario
        * */
            combinerSetup(enableCombiner, GenStopWordsJob);
            compressionSetup(codecName, conf, optParser);
            reducerSetup(numReducer, GenStopWordsJob);

        /* 
        * Generate the stop words wrt the current scenario
        * */
            try {
                LOG_GenStopWords.info("Starting job " + jobName);
                long start = new Date().getTime();
                GenStopWordsJob.waitForCompletion(true);
                long end = new Date().getTime();
                LOG_GenStopWords.info("Job " + jobName + "took "+ (end-start) + " milliseconds");
            } catch (InterruptedException | IOException | ClassNotFoundException | IllegalArgumentException Ex) {
                LOG_GenStopWords.error(GenStopWordsJob.getJobName() + ": failed" +  Ex );
            }

        /*
        * reverse sort and merge the output splits into a CSV on HDFS
        * */

        /*
        * reverse sort basic args
        * */
            conf.set("mapreduce.output.textoutputformat.separator", getTextOutputSeparator());
            conf.set("mapreduce.input.keyvaluelinerecordreader.wordA.value.separator", getTextOutputSeparator());
            Path tmpDir = new Path("reverseSort-temp-" + Integer.toString((new Random()).nextInt(2147483647)));
            String reverseJobName = "reverse-sort-" + jobName;
        /*
        * reverse sort and merge Job configuration
        *  - use an InverseMapper to swap wordA, values
        *  - use a DecreasingComparator to reverse sort by LongWritable
        *  - identity Reducer are obsoletes and don't need combiner
        * */
            Job reverseSortJob = Job.getInstance(conf);
            reverseSortJob.setJobName(reverseJobName);
            reverseSortJob.setJarByClass(reverseSort.class);
            FileInputFormat.addInputPath(reverseSortJob, GenStopWordJobOutputPath);
            FileOutputFormat.setOutputPath(reverseSortJob, tmpDir);
            reverseSortJob.setInputFormatClass(KeyValueTextInputFormat.class);
            reverseSortJob.setMapperClass(InverseMapper.class);
            reverseSortJob.setMapOutputKeyClass(Text.class);
            reverseSortJob.setMapOutputValueClass(Text.class);
            reverseSortJob.setSortComparatorClass(LongWritable.DecreasingComparator.class);
            reverseSortJob.setNumReduceTasks(1); // Hadoop 2.7.2 obsoletes IdentityReducer with the usage of one reducer

        /*
        * Reverse sort and merge with tmp file deletion
        * */
            try {
                LOG_GenStopWords.info("Starting job " + reverseJobName);
                long start = new Date().getTime();
                reverseSortJob.waitForCompletion(true);
                long end = new Date().getTime();
                LOG_GenStopWords.info("Job " + reverseJobName + "took "+ (end-start) + " milliseconds");
                String outputFile = jobOutputPath + ".csv";
                getMergeInHDFS(tmpDir, outputFile, conf);
                fs.delete(tmpDir, true);
            } catch (InterruptedException | IOException | ClassNotFoundException Ex) {
                System.err.println(Ex);
            }



        }
        return 0;
    }

    private void reducerSetup(int numReducer, Job job) throws IllegalStateException {

        if (numReducer > 0) {
            try {
                job.setNumReduceTasks(numReducer);
            } catch (IllegalStateException Ex) {
                LOG_GenStopWords.error(job.getJobName() +": reducer setup failed" +  Ex );
            }
            LOG_GenStopWords.info("Reducers = "+numReducer);
        }
    }

    private void combinerSetup(boolean enableCombiner, Job job) throws IllegalStateException {
        if (enableCombiner) {
            try {
                /* WARNING: using the GenStopWordReducer could miss a lot of values
                 * as they can be less than mInfLimit on some reducers
                 * but some to a bigger value. This is frequent with stop words
                 *  */
                job.setCombinerClass(IntSumReducer.class);
            } catch (IllegalStateException Ex) {
                LOG_GenStopWords.error(job.getJobName() +": combiner setup failed" +  Ex );
            }
        }
        LOG_GenStopWords.info("enable combiner = " + enableCombiner);
    }

    public boolean getMergeInHDFS(Path srcDir, String outputFile, Configuration config) throws
            IllegalArgumentException,
            IOException
    {
        FileSystem fs = FileSystem.get(config);

        // Check the supplied paths
        if (!(fs.exists(srcDir))) {
            LOG_GenStopWords.error("Path " + srcDir + " does not exists!");
            return false;
        }

        Path dstFile = new Path(outputFile);
        if (fs.exists(dstFile)) {
            LOG_GenStopWords.error("File " + dstFile + " already exists!");
            return false;
        }

        // merge splits into a file
        try {
            FileUtil.copyMerge(fs, srcDir, fs, dstFile, false, config, null);
            LOG_GenStopWords.info(srcDir + "->" + dstFile + " successfully merged" );
        } catch (IOException Ex) {
            LOG_GenStopWords.error(srcDir + "->" + dstFile + ": merge failed" +  Ex );
        }
        return true;
    }

    public void compressionSetup(String codecName, Configuration conf, GenericOptionsParser optParser) {
        String codecKey = "mapreduce.map.output.compress.codec";
        String compKey = "mapreduce.map.output.compress";
        switch (codecName) {
            case "Snappy":
                conf.setBoolean(compKey, true);
                conf.setClass(codecKey, SnappyCodec.class, CompressionCodec.class);
                LOG_GenStopWords.info("Enabling "+optParser.getConfiguration().get(codecKey)
                        +" compression = "+ optParser.getConfiguration().get(compKey));
                break;
            case "Lz4": // New Codec in Hadoop 2.7.2
                conf.setBoolean(compKey, true);
                conf.setClass(codecKey, Lz4Codec.class, CompressionCodec.class);
                LOG_GenStopWords.info("Enabling "+optParser.getConfiguration().get(codecKey)
                        +" compression = "+ optParser.getConfiguration().get(compKey));
                break;
            case "Gzip":
                conf.setBoolean(compKey, true);
                conf.setClass(codecKey, GzipCodec.class, CompressionCodec.class);
                LOG_GenStopWords.info("Enabling "+optParser.getConfiguration().get(codecKey)
                        +" compression = "+ optParser.getConfiguration().get(compKey));
                break;
            case "Bzip2":
                conf.setClass(codecKey, BZip2Codec.class, CompressionCodec.class);
                conf.setBoolean(compKey, true);
                LOG_GenStopWords.info("Enabling "+optParser.getConfiguration().get(codecKey)
                        +" compression = "+ optParser.getConfiguration().get(compKey));
                break;
            case "Deflate": // default codec in Hadoop 2.7.2
                conf.setBoolean(compKey, true);
                conf.setClass(codecKey, DeflateCodec.class, CompressionCodec.class);
                LOG_GenStopWords.info("Enabling "+optParser.getConfiguration().get(codecKey)
                        +" compression = "+ optParser.getConfiguration().get(compKey));
                break;
            default:
                LOG_GenStopWords.info("Compression disabled");
        }
    }
}
