package dsba.mdp.Mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by @yaj
 *
 * @date: 30/05/16.
 */
public class GenStopWordsMap extends Mapper<LongWritable, Text, Text, IntWritable> {
    private IntWritable one = new IntWritable(1);
    private Text word = new Text();

    @Override
    public void map(LongWritable offset, Text lineText, Context context)
            throws IOException, InterruptedException
    {
        StringTokenizer StrTkn =
                new StringTokenizer(lineText
                        .toString()
                        .toLowerCase()
                        .replaceAll("\\p{Punct}"," "));
        while(StrTkn.hasMoreTokens()) {
            word.set(StrTkn.nextToken());
            context.write(word, one);
        }
    }
}