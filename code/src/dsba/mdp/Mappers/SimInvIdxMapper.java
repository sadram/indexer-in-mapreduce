package dsba.mdp.Mappers;

import dsba.mdp.Drivers.TFIDIF;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Created by @yaj
 *
 * @date: 31/05/16.
 */
public class SimInvIdxMapper extends Mapper<Object, Text, Text, Text> {

    private Set<String> mStopWordsHS = new HashSet<>();
    private Text word = new Text();


    private void mStopWordsHSetter(String fileName) {
        try {
            String aStopWord;
            BufferedReader fileInputStream = new BufferedReader(new FileReader(fileName));
            /* Stop words file is sorted by (count, word) tupples to go faster
            * we keep the count for further use
            * */
            while ((aStopWord = fileInputStream.readLine().split(",")[1]) != null) {
                mStopWordsHS.add(aStopWord.toLowerCase());
            }
        } catch (IOException Ex) {
            Ex.printStackTrace();
        }
    }

    @Override
    public void map(Object key, Text value, Context context)
            throws IOException, InterruptedException
    {
        /* Build a Hash Set of stop words
        * Hash set are unordered collection of unique items
        * this data structure particularly fits our model.
        * Adding a stop word that is already present has no effect on the set.
        * */
        Configuration conf = context.getConfiguration();
        String swf = conf.get("stopwords.filename");
        mStopWordsHSetter(swf);


        /* replace punctuation, digits and non printable sequences with a unique space
        * We are counting words here :)
        * */
        String line = value.toString().toLowerCase().replaceAll("(\\p{Punct}|\\p{Digit})+"," ");
        for (String stpWrd : mStopWordsHS) {
            line = line.replaceAll(stpWrd, "");
        }

        // get the file split we are mapping from the context
        Text fileInputSplitName = new Text(((FileSplit) context.getInputSplit()).getPath().getName());

        // emit (word, fileSplit) and update the global input word counter
        StringTokenizer itr = new StringTokenizer(line);
        while (itr.hasMoreTokens()) {
            word.set(itr.nextToken());
            if(word != null) {
                context.write(word, fileInputSplitName);
                Counter counter = context.getCounter(TFIDIF.CountersEnum.class.getName(), TFIDIF.CountersEnum.INPUT_WORDS.toString());
                counter.increment(1);
            }
        }
    }
}
