package dsba.mdp.WordCountsExamples;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.StringTokenizer;


/**
 * Created by @yaj
 *
 * @date: 25/05/16.
 */


public class WordCountCSNP extends Configured implements Tool {
    private static final Logger LOG_MAIN = Logger.getLogger(WordCountCSNP.class);

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new WordCountCSNP(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Job wordCountJobCSNP = Job.getInstance(getConf(), "Word Count CS NP");
        wordCountJobCSNP.setJarByClass(this.getClass());

        FileInputFormat.setInputPaths(wordCountJobCSNP, new Path(args[0]));
        FileOutputFormat.setOutputPath(wordCountJobCSNP, new Path(args[1]));

        wordCountJobCSNP.setMapperClass(MapCSNP.class);
        wordCountJobCSNP.setReducerClass(ReduceCSNP.class);
        wordCountJobCSNP.setOutputKeyClass(Text.class);
        wordCountJobCSNP.setOutputValueClass(IntWritable.class);

        return wordCountJobCSNP.waitForCompletion(true) ? 0 : 1;
    }



    public static class MapCSNP extends Mapper<LongWritable, Text, Text, IntWritable>
    {
        private static final Logger LOG_MAPCSNP = Logger.getLogger(MapCSNP.class);
        private static IntWritable one = new IntWritable(1);
        private boolean caseSensitive = false;
        private boolean filterPunct = true;
        private Text word = new Text();

        protected void setup(MapCSNP.Context context)
                throws IOException, InterruptedException
        {
            Configuration config = context.getConfiguration();

            this.caseSensitive = config.getBoolean("WordCountCSNP.case.sensitive", false);
            this.filterPunct = config.getBoolean("WordCountCSNP.filter.punct", true);
            LOG_MAPCSNP.debug("this.caseSensitive = " + this.caseSensitive);
            LOG_MAPCSNP.debug("this.filterPunct = " + this.filterPunct);

        }

        public void map(LongWritable offset, Text lineText, Context context)
                throws IOException, InterruptedException
        {
            StringTokenizer itr;
            if (this.caseSensitive && !this.filterPunct) {
                itr = new StringTokenizer(lineText.toString());
            } else if(!this.filterPunct) {
                itr = new StringTokenizer(lineText.toString().toLowerCase());
            } else {
                itr = new StringTokenizer(lineText.toString().toLowerCase().replaceAll("\\p{Punct}",""));
            }

            while(itr.hasMoreTokens()) {
                word.set(itr.nextToken());
                context.write(word, one);

            }
        }
    }

    public static class ReduceCSNP extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(Text word, Iterable<IntWritable> counts, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable count: counts) {
                sum += count.get();
            }
            context.write(word, new IntWritable(sum));
        }
    }
}
