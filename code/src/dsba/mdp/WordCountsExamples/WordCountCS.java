package dsba.mdp.WordCountsExamples;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import org.apache.log4j.Logger;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;


/**
 * Created by @yaj
 *
 * @date: 25/05/16.
 */


public class WordCountCS extends Configured implements Tool {
    private static final Logger LOG = Logger.getLogger(WordCountCS.class);

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new WordCountCS(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Job wordCountJobCS = Job.getInstance(getConf(), "Word Count CS");
        wordCountJobCS.setJarByClass(this.getClass());

        FileInputFormat.setInputPaths(wordCountJobCS, new Path(args[0]));
        FileOutputFormat.setOutputPath(wordCountJobCS, new Path(args[1]));

        wordCountJobCS.setMapperClass(MapCS.class);
        wordCountJobCS.setReducerClass(ReduceCS.class);
        wordCountJobCS.setOutputKeyClass(Text.class);
        wordCountJobCS.setOutputValueClass(IntWritable.class);

        return wordCountJobCS.waitForCompletion(true) ? 0 : 1;
    }



    public static class MapCS extends Mapper<LongWritable, Text, Text, IntWritable> {
        private static IntWritable one = new IntWritable(1);

        // flag to match command-line opt
        private boolean caseSensitive = false;

        private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

        protected void setup(Context context)
                throws IOException, InterruptedException
        {
            Configuration config = context.getConfiguration();
            // gets the value of a command line string else sets it up to false
            this.caseSensitive = config.getBoolean("WordCountCS.case.sensitive", false);
            LOG.debug("this.caseSensitive = " + this.caseSensitive);

        }

        public void map(LongWritable offset, Text lineText, Context context)
                throws IOException, InterruptedException
        {
            String line = lineText.toString();
            LOG.debug("this.caseSensitive = " + this.caseSensitive);

            if(this.caseSensitive == false) {
                line = line.toLowerCase();
            }

            Text currentWord;
            for (String word: WORD_BOUNDARY.split(line)) {
                if(word.isEmpty()) { continue; }
                currentWord = new Text((word));
                context.write(currentWord, one);
            }
        }
    }


    public static class ReduceCS extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(Text word, Iterable<IntWritable> counts, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable count: counts) {
                sum += count.get();
            }
            context.write(word, new IntWritable(sum));
        }
    }
}
