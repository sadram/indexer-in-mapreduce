package dsba.mdp.WordCountsExamples;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import org.apache.log4j.Logger;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

/**
 * Created by @yaj
 *
 * @date: 25/05/16.
 */


public class WordCount extends Configured implements Tool {
    // Create a logger for debugging
    private static final Logger LOG = Logger.getLogger(WordCount.class);

    // main will schedule the job
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new WordCount(), args);
        System.exit(res);
    }

    /* as we implement Tool we must implement the run abstract method
    * to configure the job and schedule it for running
    * we could use multiple runners for different jobs */
    @Override
    public int run(String[] args) throws Exception {

        // create an instance of instance the job and get its configuration
        // "Word Count v0" will be its name in the Resource Manager
        Job wordCountJob = Job.getInstance(getConf(), "Word Count v0");

        // the jar to use (we are in a static context due main
        // which is why we use this and not WordCount.getClass()
        wordCountJob.setJarByClass(this.getClass());

        // input and output path base on commandline arguments
        FileInputFormat.setInputPaths(wordCountJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(wordCountJob, new Path(args[1]));

//        Mapper and Reducer setup are optional
        wordCountJob.setMapperClass(WordCoundMapper.class);
        wordCountJob.setReducerClass(WordCountReducer.class);

        // wordA, value output types
        wordCountJob.setOutputKeyClass(Text.class);
        wordCountJob.setOutputValueClass(IntWritable.class);

        // launch the job (true = verbose map and reduce progress)
        return wordCountJob.waitForCompletion(true) ? 0 : 1;
    }


    /* Map transforms input k,v = LongWritable, Text -> output k,v = Text, IntWritable
    * where Text while be the wordA (our word) and IntWritable 1
    * output is passed to the Reducer */
    public static class WordCoundMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private static IntWritable one = new IntWritable(1);

        // whitespace(s) word boundary whitespace(s)
        private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

        /* map is called for every wordA, value received as input:
        * wordA = the offset of the first character in the current line
        * value = the entire line  */
        public void map(LongWritable offset, Text lineText, Context context)
                throws IOException, InterruptedException {
            // parse line word per word and emit (word, 1)
            String line = lineText.toString();
            Text currentWord;
            for (String word: WORD_BOUNDARY.split(line)) {
                if(word.isEmpty()) { continue; }
                currentWord = new Text((word));
                // emit k,v
                context.write(currentWord, one);
            }
        }
    }


    /* Reduce called for each wordA received by the mapper
    * k,v emitted from the Map class must match Reduce class input k,v types */
    public static class WordCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(Text word, Iterable<IntWritable> counts, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable count: counts) {
                sum += count.get();
            }
            context.write(word, new IntWritable(sum));
        }
    }


}
