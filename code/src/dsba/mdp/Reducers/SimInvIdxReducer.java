package dsba.mdp.Reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.StringUtils;

import java.io.IOException;
import java.util.HashSet;

/**
 * Created by @yaj
 *
 * @date: 31/05/16.
 */
public class SimInvIdxReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text word, Iterable<Text> documents, Context context)
            throws IOException, InterruptedException
    {
        HashSet<String> wordInvIdxReducedHS = new HashSet<>();
        for(Text docs: documents) {
            if(docs != null) {
                wordInvIdxReducedHS.add(docs.toString());
            }
        }
        context.write(word, new Text(StringUtils.join(",", wordInvIdxReducedHS)));
    }
}