package dsba.mdp.Reducers;

import dsba.mdp.Drivers.TFIDIF;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by @yaj
 *
 * @date: 31/05/16.
 */
public class UniqInvIdxReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException
    {
        // To get a count of unique words across all the documents
        ArrayList<String> docContainingWord = new ArrayList<>();
        // To get a count of unique words per document
        // HashSet<String> docContainingWord = new HashSet<>();
        for(Text docs: values) {
            if(docs != null) {
                docContainingWord.add(docs.toString());
            }
        }
        /* if the size of docContainingWord is 1 then the word is:
         * - unique for all the document corpus in case docContainingWord is an ArrayList
         * - only appears in one document of the corpus in case docContainingWord is a HashSet
         * Hence we emit it/
         * */
        if (docContainingWord.size() == 1) {
            // Count unique word per documents. Will be printed via the context in the main
            context.getCounter(TFIDIF.CountersEnum.UNIQ_WORDS).increment(1);
            context.write(key, new Text(StringUtils.join(",", docContainingWord)));
        }
    }
}