package dsba.mdp.Reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by @yaj
 *
 * @date: 31/05/16.
 */
public class FreqInvIdxReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text word, Iterable<Text> documents, Context context)
            throws IOException, InterruptedException
    {
        ArrayList<String> docListPerWord = new ArrayList<>();
        for(Text doc: documents) {
            docListPerWord.add(doc.toString());
        }

        HashSet<String> wordFreqPerDoc = new HashSet<>(docListPerWord);
        HashSet<String> invIndexWithFreq = new HashSet<>();
        for(String doc: wordFreqPerDoc) {
            if(doc != null) {
                invIndexWithFreq.add(doc
                        + "#"
                        + Collections.frequency(docListPerWord, doc)
                        // add to set val#freq(val in values)/size(values)
                        //+ (Collections.frequency(docListPerWord, val) / (double) docListPerWord.size())
                );
            }
        }
        // output word:doc#f1,doc#f2,...
        context.write(word, new Text(StringUtils.join(",", invIndexWithFreq)));
    }
}