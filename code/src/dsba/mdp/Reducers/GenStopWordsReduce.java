package dsba.mdp.Reducers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by @yaj
 *
 * @date: 30/05/16.
 */
public class GenStopWordsReduce extends Reducer<Text, IntWritable, Text, IntWritable> {
    private int mInfLimit = 4000;
    // private long totalWord = 0L;

    public int getInfLimit() {
        return mInfLimit;
    }

    public void setInfLimit(int infLimit) {
        mInfLimit = infLimit;
    }

    private IntWritable result = new IntWritable();


    protected void reduce(Text word, Iterable<IntWritable> counts, Context context)
            throws IOException, InterruptedException
    {
        int sum = 0;

        for (IntWritable count : counts) {
            sum += count.get();
        }
        if (sum > mInfLimit) {
            result.set(sum);
            context.write(word, result);
        }
    }
}
