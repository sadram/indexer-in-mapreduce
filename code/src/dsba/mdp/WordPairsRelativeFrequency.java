package dsba.mdp;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 * Created by @yaj
 *
 * @date: 02/06/16.
 */
public class WordPairsRelativeFrequency {

    public static void usage() {
        System.err.println("hadoop WordPairsRelativeFrequency.jar <inputPath> <outputPath>");
        System.exit(-1);
    }

    public static void main(String[] args) throws IOException {

        if (args.length != 3) {
            usage();
        }
        Configuration RFConf = new Configuration();
        Job jobWordPairsRF = new Job(RFConf);
        jobWordPairsRF.setJarByClass(WordPairsRelativeFrequency.class);
        jobWordPairsRF.setNumReduceTasks(1);

        jobWordPairsRF.setMapperClass(RFMapper.class);
        jobWordPairsRF.setCombinerClass(RFCombiner.class);
        jobWordPairsRF.setReducerClass(RFReducer.class);

        jobWordPairsRF.setInputFormatClass(TextInputFormat.class);
        jobWordPairsRF.setOutputFormatClass(TextOutputFormat.class);

        FileSystem fs = FileSystem.get(RFConf);
        String inputPath = args[0] + "/" + args[1];
        if(!fs.exists(new Path(inputPath))) {
            System.err.println(inputPath + " does not exists");
            System.exit(-1);
        }

        String outputPath = args[0] + "/" + args[2];
        if(fs.exists(new Path(outputPath))) {
            System.err.println(outputPath + " exists");
            System.exit(-1);
        }

        FileInputFormat.addInputPath(jobWordPairsRF, new Path(inputPath));
        FileOutputFormat.setOutputPath(jobWordPairsRF, new Path(outputPath));

        try {
            System.exit((jobWordPairsRF.waitForCompletion(true)?0:1));
        } catch (InterruptedException e) {
            System.err.println("Job interrupted " + jobWordPairsRF.getJobName());
        } catch (ClassNotFoundException e) {
            System.err.println("Class not found");
        }
    }

    public class RFMapper extends Mapper<LongWritable, Text, Text, Text> {

        private final Pattern NON_ALNUM = Pattern.compile("[\\P{IsAlphabetic}\\p{IsDigit}]+");
        private final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
        private final Text ONE = new Text("1");

        @Override
        protected void map(LongWritable index, Text line, Context context) throws IOException, InterruptedException {
            // fastest possible regex for word spliting
            String words[] = WORD_BOUNDARY.split(NON_ALNUM.matcher(line.toString().toLowerCase()).replaceAll(" "));
            for (String word : words)
            {
                if (!word.isEmpty()) {
                    int count = 0;
                    for (String nextWord : words) {
                        if(!nextWord.isEmpty() && !nextWord.equals(word)) {
                            context.write(new Text(word + "," + nextWord), ONE);
                            count++;
                        }
                    }
                    context.write(new Text(word + ",*" ), new Text(String.valueOf(count)));
                }
            }

        }
    }


    public class RFCombiner extends Reducer<Text, Text, Text, Text> {
        @Override
        protected void reduce(Text wordPairs, Iterable<Text> counts, Context context) throws IOException, InterruptedException {
            long sum = 0;
            for (Text cnt: counts) {
                if (cnt != null) {
                    sum += Integer.parseInt(cnt.toString());
                }
            }
            context.write(wordPairs, new Text(String.valueOf(sum)));
        }
    }

    public class RFReducer extends Reducer<Text, Text, Text, Text> {

        protected final Pattern GLOB_PAIR = Pattern.compile(".*\\*");
        protected double sigma_AB_i = 0;
        protected TreeSet<Pair> TOP100PAIRS_FIFO = new TreeSet<>();


        @Override
        protected void reduce(Text wordPairs, Iterable<Text> counts, Context context) throws IOException, InterruptedException {
            long sum = 0;

            for (Text cnt : counts) {
                if (cnt != null) {
                    sum += Integer.parseInt(cnt.toString());
                }
            }

            String wordPairsStr = wordPairs.toString();

            if (GLOB_PAIR.matcher(wordPairsStr).find()) {
                sigma_AB_i = sum;
            } else {
                String[] wordAB = wordPairsStr.split(",");
                TOP100PAIRS_FIFO.add(new Pair(sum / sigma_AB_i, wordAB[0], wordAB[1]));

                if (TOP100PAIRS_FIFO.size() > 100) {
                    TOP100PAIRS_FIFO.pollFirst();
                }
            }
        }

        protected void cleanup(Context context) throws IOException, InterruptedException {
            while(!TOP100PAIRS_FIFO.isEmpty()) {
                Pair highestRFPairs = TOP100PAIRS_FIFO.pollLast();
                context.write(
                        new Text(highestRFPairs.wordA + "+" + highestRFPairs.wordB),
                        new Text(String.valueOf(highestRFPairs.relativeFrequency))
                );
            }
        }


        protected class Pair implements Comparable<Pair> {

            public double relativeFrequency;
            public String wordA;
            public String wordB;

            Pair(double relativeFrequency, String wordA, String wordB) {
                this.relativeFrequency = relativeFrequency;
                this.wordA = wordA;
                this.wordB = wordB;
            }

            @Override
            public int compareTo(Pair pair) {
                return (this.relativeFrequency >= relativeFrequency ? 1 : -1);
            }
        }
    }

}
