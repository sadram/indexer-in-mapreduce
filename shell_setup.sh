add_to_path() {
# safely add a path to the PATH environment variable
  case ":$PATH:" in
      *":$1:"*) :;; # already here
      *) PATH="$PATH:$1" ;;
  esac
  export PATH
  return $?
}

export PROJHOME=${HOME}/devel/DSBA/mdp/Assign1
export PROJBINHOME=${PROJHOME}/bin
export SCRIPTDIR=${PROJHOME}/scripts
export HADOOP_HOME=${PROJBINHOME}/hadoop
add_to_path "${HADOOP_HOME}/sbin"
add_to_path "${HADOOP_HOME}/bin"
source ${SCRIPTDIR}/env-setup.sh
export REPORTDIR=${PROJHOME}/report
export RESULTDIR=${REPORTDIR}/results
#export HADOOP_MAPRED_HOME=$HADOOP_HOME
#export HADOOP_COMMON_HOME=$HADOOP_HOME
#export HADOOP_HDFS_HOME=$HADOOP_HOME
#export YARN_HOME=$HADOOP_HOME
#export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
#export HDFSPATH=$PROJBINHOME/hadoopinfra/hdfs
#export HDFSTMP=$HDFSPATH/tmp
#export NAMENODEPATH=$HDFSPATH/namenode
#export DATANODEPATH=$HDFSPATH/datanode
#export TARGETDIR="${PROJHOME}/bin/installers"
